package jp.ac.uec.hc.cas.rovermonitor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Takato on 2016/07/28.
 */
public class TransmissionClient extends Thread {
    Socket socket = null;
    BufferedReader in = null;
    BufferedWriter out = null;

    private int port;
    private String ipAddress;

    private boolean waiting;
    private boolean c = true;

    private Queue<String> messageQueue;

    ArrayList<ClientSnedTime> sendTimeArrayList = new ArrayList<ClientSnedTime>();

    private List<Observer> observers = new CopyOnWriteArrayList<Observer>();

    public TransmissionClient(String destinationIpAddress, int portNumber) {
        ipAddress = destinationIpAddress;
        port = portNumber;
        waiting = true;
        messageQueue = new ArrayDeque<String>();
    }

    public void send(String tag, String message) {
        boolean tmpMatch = false;

        for(int i = 0; i < sendTimeArrayList.size(); i++) {
            if(tag == sendTimeArrayList.get(i).tag) {
                if(!sendTimeArrayList.get(i).toGivePermissionSend()) {
                    return;
                }
                tmpMatch = true;
                break;
            }
        }

        if(!tmpMatch) {
            sendTimeArrayList.add(new ClientSnedTime(tag));
        }
        messageQueue.add(message);
    }

    @Override
    public void run() {
        long sendTime = System.currentTimeMillis();
        while (c) {
            try {
                socket = new Socket();
                socket.connect(new InetSocketAddress(ipAddress, port));

                out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                while (waiting) {
                    if (in.ready()) {
                        String responce = in.readLine();
                            notifyObservers(responce);
                    }

                    long now = System.currentTimeMillis();
                    if (messageQueue.peek() != null) {
                        out.write(messageQueue.poll());
                        out.newLine();
                        out.flush();
                        sendTime = now;
                    } else if (now - sendTime > 1000) {
                        out.write("ping");
                        out.newLine();
                        out.flush();
                        sendTime = now;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (socket != null) {
                        socket.close();
                    }
                    if (in != null) {
                        in.close();
                    }
                    if (out != null) {
                        out.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    Thread.sleep(10000);
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private void notifyObservers(String message) {
        for(Observer observer: observers)
            observer.updateMessage(message);
    }

    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    public void destroyTransmission() {
        waiting = false;
        c = false;
        try {
            if (socket != null) {
                socket.close();
            }
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class ClientSnedTime {
    long sentTimeMillis;
    String tag;

    public ClientSnedTime(String tag) {
        this.tag = tag;
        sentTimeMillis = System.currentTimeMillis();
    }

    public boolean toGivePermissionSend() {
        long now = System.currentTimeMillis();
        if(now - this.sentTimeMillis > 200) {
            this.sentTimeMillis = now;
            return true;
        } else {
            return false;
        }
    }
}