package jp.ac.uec.hc.cas.rovermonitor;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ScrollingTabContainerView;
import android.util.Log;
import android.util.StringBuilderPrinter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Timer;
import java.util.TimerTask;

public class PrototypeCongrolorActivity extends AppCompatActivity {
    TextView statusText;
    Handler mHandler;
    ScrollView statusScrollView;
    boolean advancingFlag = false; // 前進モード

    boolean yetCommunicationFlag = true;

    int color;
    Drawable backgroundDrawable;
    ActionBar actionBar;

    Globals globals;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prototype_congrolor);
        Intent intent = getIntent();
        mHandler = new Handler();

        globals = (Globals) this.getApplication();

        actionBar = getSupportActionBar();

        Timer communicationTimer = new Timer(true);
        communicationTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (yetCommunicationFlag) {
                            setTitle(getString(R.string.yetConnect) + "  " + globals.getSSID());
                            color = R.color.action_bar_green;
                            backgroundDrawable = getApplicationContext().getResources().getDrawable(color);
                            actionBar.setBackgroundDrawable(backgroundDrawable);
                        } else {
                            if (globals.getRoverCommunicationStatus() == globals.FULL_COMMUNICATION) {
                                setTitle(getString(R.string.connect) + "  " + globals.getSSID() + " (" + globals.getRssi() + "[dBm])");
                                color = R.color.action_bar_blue;
                                backgroundDrawable = getApplicationContext().getResources().getDrawable(color);
                                actionBar.setBackgroundDrawable(backgroundDrawable);
                            } else {
                                setTitle(getString(R.string.disconnect) + "  " + globals.getSSID());
                                color = R.color.action_bar_red;
                                backgroundDrawable = getApplicationContext().getResources().getDrawable(color);
                                actionBar.setBackgroundDrawable(backgroundDrawable);
                            }
                        }

                    }
                });
            }
        }, 0, 5000);

        globals.addObserver(observer);

        statusText = (TextView) findViewById(R.id.statusText);
        statusScrollView = (ScrollView) findViewById(R.id.statusScrollview);

        final VerticalSeekBar leftMotorSeekBar = (VerticalSeekBar) findViewById(R.id.leftMotorSeekBar);
        final VerticalSeekBar rightMotorSeekBar = (VerticalSeekBar) findViewById(R.id.rightMotorSeekBar);
        final TextView rightMotorView = (TextView) findViewById(R.id.rightMotorView);
        rightMotorView.setText("0");
        final TextView leftMotorView = (TextView) findViewById(R.id.leftMotorView);
        leftMotorView.setText("0");

        leftMotorSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int nowProgress = leftMotorSeekBar.getProgress();
                globals.send("l_motor", "l_motor " + (nowProgress - 100));
                leftMotorView.setText(String.valueOf(nowProgress - 100));
                if(advancingFlag) {
                    rightMotorSeekBar.setProgressAndThumb(nowProgress);
                    globals.send("r_motor", "r_motor " + (nowProgress - 100));
                    rightMotorView.setText(String.valueOf(nowProgress - 100));
                }
                statusScrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                globals.send("l_motor0", "l_motor 0");
                leftMotorSeekBar.setProgressAndThumb(100);
                leftMotorView.setText("0");
                if(advancingFlag) {
                    rightMotorSeekBar.setProgressAndThumb(100);
                    globals.send("r_motor0", "r_motor 0");
                    rightMotorView.setText("0");
                }
            }
        });

        rightMotorSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int nowProgress = rightMotorSeekBar.getProgress();
                globals.send("r_motor", "r_motor " + (nowProgress - 100));
                rightMotorView.setText(String.valueOf(nowProgress - 100));
                if(advancingFlag) {
                    leftMotorSeekBar.setProgressAndThumb(nowProgress);
                    globals.send("l_motor", "l_motor " + (nowProgress - 100));
                    leftMotorView.setText(String.valueOf(nowProgress - 100));
                }
                statusScrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                globals.send("r_motor0", "r_motor 0");
                rightMotorSeekBar.setProgressAndThumb(100);
                rightMotorView.setText("0");
                if(advancingFlag) {
                    leftMotorSeekBar.setProgressAndThumb(100);
                    globals.send("l_motor0", "l_motor 0");
                    leftMotorView.setText("0");
                }
            }
        });


        final SeekBar paraServoSeekBar = (SeekBar) findViewById(R.id.paraServoSeekBar);
        final TextView paraServoView = (TextView) findViewById(R.id.paraServoView);
        paraServoView.setText("0");
        paraServoSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                paraServoView.setText(String.valueOf(paraServoSeekBar.getProgress()));
                if((paraServoSeekBar.getProgress() == 0) || (paraServoSeekBar.getProgress() == 180)) {
                    globals.send("p_servo_e", "p_servo " + (paraServoSeekBar.getProgress()));
                    return;
                }
                globals.send("p_servo", "p_servo " + (paraServoSeekBar.getProgress()));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        final SeekBar stabiServoSeekBar = (SeekBar) findViewById(R.id.stabiServoSeekBar);
        final TextView stabiServoView = (TextView) findViewById(R.id.stabiServoView);
        stabiServoView.setText("180");
        stabiServoSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                stabiServoView.setText(String.valueOf(stabiServoSeekBar.getProgress()));
                if((stabiServoSeekBar.getProgress() == 0) || (stabiServoSeekBar.getProgress() == 180)) {
                    globals.send("t_servo_e", "t_servo " + (stabiServoSeekBar.getProgress()));
                    return;
                }
                globals.send("t_servo", "t_servo " + (stabiServoSeekBar.getProgress()));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        Button leftMotorStoppButton = (Button) findViewById(R.id.leftMotorStopButton);
        leftMotorStoppButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                globals.send("l_motor_s", "l_motor s");
            }
        });

        Button rightMotorStopButton = (Button) findViewById(R.id.rightMotorStopButton);
        rightMotorStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                globals.send("r_motor_s", "r_motor s");
            }
        });

        final EditText commandText = (EditText) findViewById(R.id.commandText);
        Button sendButton = (Button) findViewById(R.id.sendButton);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                globals.send("command", commandText.getText().toString());
                statusText.setText(statusText.getText().toString() + "SEND: " + commandText.getText().toString() + "\n");
                statusScrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });

        Button turnLeftButton = (Button) findViewById(R.id.turnLeftButton);
        turnLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                globals.send("turn_left", "turn left");
            }
        });

        Button turnRightButton = (Button) findViewById(R.id.turnRightButton);
        turnRightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                globals.send("turn_Right", "turn right");
            }
        });

        final Button advancingButton = (Button) findViewById(R.id.advancingButton);
        advancingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(advancingFlag) {
                    advancingFlag = false;
                    advancingButton.setText(getText(R.string.removeAdvanceingMode));
                } else {
                    advancingFlag = true;
                    advancingButton.setText(getText(R.string.advancingMode));
                }
            }
        });

        Button startSequenceButton = (Button) findViewById(R.id.startSequenceButton);
        startSequenceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                globals.send("startSequence", "start allsequence");
            }
        });

        Button endSequenceButton = (Button) findViewById(R.id.endSequenceButton);
        endSequenceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                globals.send("stopSequence", "stop allsequence");
            }
        });

    }

    private Observer observer = new Observer() {
        @Override
        public void updateMessage(String message) {
            yetCommunicationFlag = false;
            final String receivedMessage = message;
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if(!receivedMessage.equals("ping")) {
                        //statusText.setText(statusText.getText().toString() + receivedMessage + "\n");
                        //statusScrollView.fullScroll(ScrollView.FOCUS_DOWN);
                    }
                }
            });
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        globals.removeObserver(observer);
    }
}
