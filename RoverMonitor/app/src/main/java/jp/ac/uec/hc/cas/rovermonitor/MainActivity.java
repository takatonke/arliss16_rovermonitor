package jp.ac.uec.hc.cas.rovermonitor;

import android.content.Intent;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.net.InetAddress;
import java.util.Calendar;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    Button ipAddressButton, controlorButton, monitorButton;
    Globals globals;
    boolean communicationStatus = false;
    Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mHandler = new Handler();

        // バージョン表示
        final TextView versionView = (TextView) findViewById(R.id.versionView);
        versionView.setText("Version : " + BuildConfig.VERSION_NAME + "(" + getString(R.string.versionName) + ")");
        versionView.setClickable(true);
        versionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //UDP_Button.setEnabled(true);
                versionView.setClickable(false);
            }
        });

        globals = (Globals) this.getApplication();
        globals.setWifiManager((WifiManager) getSystemService(WIFI_SERVICE));

        final EditText ipAddressText = (EditText) findViewById(R.id.ipAddress);
        ipAddressText.setText("192.168.43.1");

        ipAddressButton = (Button) findViewById(R.id.ipAddressButton);
        ipAddressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!communicationStatus) {
                    globals.setIpAddress(ipAddressText.getText().toString());
                    globals.startTcpCommunication();
                    controlorButton.setEnabled(true);
                    monitorButton.setEnabled(true);
                    ipAddressButton.setText(getText(R.string.end));
                    communicationStatus = true;
                } else {
                    globals.endTcpCommunication();
                    controlorButton.setEnabled(false);
                    monitorButton.setEnabled(false);
                    ipAddressButton.setText(getText(R.string.start));
                    communicationStatus = false;
                }
            }
        });

        controlorButton = (Button) findViewById(R.id.controlorButton);
        controlorButton.setEnabled(false);
        controlorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PrototypeCongrolorActivity.class);
                startActivity(intent);
            }
        });

        monitorButton = (Button) findViewById(R.id.monitorButton);
        monitorButton.setEnabled(false);
        monitorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MonitorActivity.class);
                intent.putExtra("ipAddress", ipAddressText.getText().toString());
                startActivity(intent);
            }
        });

        final TextView dayView = (TextView) findViewById(R.id.dayView);
        final String[] week_name = {"日", "月", "火", "水", "木", "金", "土"};
        Timer communicationTimer = new Timer(true);
        communicationTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Calendar calendar = Calendar.getInstance();
                        int year = calendar.get(Calendar.YEAR);
                        int month = calendar.get(Calendar.MONTH) + 1;
                        int day = calendar.get(Calendar.DATE);
                        int hour = calendar.get(Calendar.HOUR_OF_DAY);
                        int minute = calendar.get(Calendar.MINUTE);
                        int second = calendar.get(Calendar.SECOND);
                        int week = calendar.get(Calendar.DAY_OF_WEEK) - 1;

                        dayView.setText(String.valueOf(year) + "年" + String.valueOf(month) + "月"
                                + String.valueOf(day) + "日(" + week_name[week] + ")\n  " + String.valueOf(hour) + "時"
                                + String.valueOf(minute) + "分" + String.valueOf(second) + "秒");
                    }
                });
            }
        }, 0, 1000);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        globals.endTcpCommunication();
    }
}