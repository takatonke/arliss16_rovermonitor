package jp.ac.uec.hc.cas.rovermonitor;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Takato on 2016/08/13.
 */
public class Arc extends View {
    private final Paint paint;
    private RectF rect;

    // Arcの幅
    private final int strokeWidth = 60;
    // Animation開始位置
    private static final int AngleTarget = 270;
    // 初期Angle
    private float angle = 0;
    // 半径
    private float radius;

    public Arc(Context context, AttributeSet attrs) {
        super(context, attrs);

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(strokeWidth);

        // Ardの色
        paint.setColor(Color.argb(255, 0, 0, 255));
        // Arcの範囲
        rect = new RectF();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Canvas中心
        float x = canvas.getWidth() / 2;
        float y = canvas.getHeight() / 2;
        radius = canvas.getWidth() / 4;

        // 円弧の領域
        rect.set(x - radius, y - radius, x + radius, y + radius);
        // 円弧の描画
        canvas.drawArc(rect, AngleTarget, angle, false, paint);
    }

    // 現在のangle
    public float getAngle() {
        return angle;
    }

    // angleのset
    public void setAngle(float angle) {
        this.angle = angle;
    }

    public void setColor(int r, int g, int b, int a) {
        paint.setColor(Color.argb(a, r, g, b));
    }
}
