package jp.ac.uec.hc.cas.rovermonitor;

/**
 * Created by Takato on 2016/05/28.
 */
public interface Observer {
    void updateMessage(String message);
}
