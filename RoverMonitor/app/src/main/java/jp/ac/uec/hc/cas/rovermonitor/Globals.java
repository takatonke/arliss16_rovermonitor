package jp.ac.uec.hc.cas.rovermonitor;

import android.app.Application;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Takato on 2016/08/24.
 */
public class Globals extends Application {
    private TransmissionClient transmissionClient = null;
    private String ipAddress = "192.168.43.1";
    public static final int TCP_PORT = 5555;

    Timer receivedTimer = null;

    public static final int NOT_COMMUNICATION = 0;
    public static final int NOT_CIRCUIT_COMMUNICATION = 1;
    public static final int FULL_COMMUNICATION = 2;
    private int roverCommunicationStatus = NOT_COMMUNICATION;

    private double roverLatitude = 0;
    private double roverLongitude = 0;
    private int roverBattery = 0;
    private int distance = 0;
    private int roverMode = 0;
    private double circuitBattery = 0;
    private int roverStatus = 0;

    private WifiManager wifiManager;
    private WifiInfo wifiInfo;

    private Observer observer = new Observer() {
        @Override
        public void updateMessage(String message) {
            roverCommunicationStatus = FULL_COMMUNICATION;
            wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
            wifiInfo = wifiManager.getConnectionInfo();

            try {
                receivedTimer.cancel();
                receivedTimer = null;
            } catch (Exception e) {
                e.printStackTrace();
            }

            String[] command = message.split(" ");
            if(command.length == 2) {
                switch (command[0]) {
                    case "latitude":
                        roverLatitude = Double.parseDouble(command[1]);
                        break;
                    case "longitude":
                        roverLongitude = Double.parseDouble(command[1]);
                        break;
                    case "roverBattery":
                        roverBattery = Integer.parseInt(command[1]);
                        break;
                    case "distance":
                        distance = Integer.parseInt(command[1]);
                        break;
                    case "mode":
                        roverMode = Integer.parseInt(command[1]);
                        break;
                    case "circuit":
                        circuitBattery = Double.parseDouble(command[1]);
                        break;
                    case "status":
                        roverStatus = Integer.parseInt(command[1]);
                        break;
                }
            }

            receivedTimer = new Timer(true);
            receivedTimer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    cancel();
                    roverCommunicationStatus = NOT_COMMUNICATION;
                }
            }, 1000, 5000);
        }
    };

    public void setIpAddress(String address) {
        ipAddress = address;
        return;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void startTcpCommunication() {
        transmissionClient = new TransmissionClient(ipAddress, TCP_PORT);
        transmissionClient.addObserver(observer);
        transmissionClient.start();
    }

    public void endTcpCommunication() {
        transmissionClient.destroyTransmission();
    }

    public void addObserver(Observer observer) {
        transmissionClient.addObserver(observer);
    }

    public void removeObserver(Observer observer) {
        transmissionClient.removeObserver(observer);
    }

    public void send(String tag, String message) {
        transmissionClient.send(tag, message);
    }

    public void setRoverCommunicationStatus(int status) {
        roverCommunicationStatus = status;
    }

    public int getRoverCommunicationStatus() {
        return roverCommunicationStatus;
    }

    public double getRoverLatitude() {
        return roverLatitude;
    }

    public double getRoverLongitude() {
        return roverLongitude;
    }

    public int getRoverBattery() {
        return roverBattery;
    }

    public String getSSID() {
        return wifiInfo.getSSID();
    }

    public void setWifiManager(WifiManager Manager) {
        wifiManager = Manager;
        wifiInfo = wifiManager.getConnectionInfo();
    }

    public int getRssi() {
        return wifiInfo.getRssi();
    }

    public int getRssiLevel() {
        return wifiManager.calculateSignalLevel(getRssi(), 5);
    }

    public int getDistance() {
        return distance;
    }

    public int getRoverMode() {
        return roverMode;
    }

    public double getCircuitBattery() {
        return circuitBattery;
    }

    public int getRoverStatus() {
        return roverStatus;
    }
}
