package jp.ac.uec.hc.cas.rovermonitor;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MonitorActivity extends AppCompatActivity {
    private Arc batteryArc;
    private int animationPeriod = 2000;
    private Handler mHandler;
    Globals globals;
    Drawable backgroundDrawable;
    ActionBar actionBar;
    int color;
    int count = 0;
    long startTime;
    boolean startFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitor);

        mHandler = new Handler();
        globals = (Globals) this.getApplication();

        final Button sequenceStartButton = (Button)findViewById(R.id.sequenceStartButton);
        sequenceStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                globals.send("startSequence", "start allsequence");
                startTime = System.currentTimeMillis();
                startFlag = true;
                sequenceStartButton.setEnabled(false);
            }
        });

        actionBar = getSupportActionBar();

        batteryArc = (Arc) findViewById(R.id.batteryArc);

        final TextView roverLatitudeView = (TextView) findViewById(R.id.roverLatitudeView);
        final TextView roverLongitudeView = (TextView) findViewById(R.id.roverLongitudeView);
        final TextView roverBatteryView = (TextView) findViewById(R.id.batteryView);
        final TextView sequenceTimeView = (TextView)findViewById(R.id.sequenceTimeView);
        final TextView distanceView = (TextView)findViewById(R.id.distanceView);
        final TextView circuitBatteryView = (TextView)findViewById(R.id.circuitBatteryView);
        final TextView statusView = (TextView)findViewById(R.id.statusView);

        Timer communicationTimer = new Timer(true);
        communicationTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        count++;
                        if(count == 5) {
                            if (globals.getRoverBattery() == 0) {
                                setTitle(getString(R.string.yetConnect) + "  " + globals.getSSID());
                                color = R.color.action_bar_green;
                                backgroundDrawable = getApplicationContext().getResources().getDrawable(color);
                                actionBar.setBackgroundDrawable(backgroundDrawable);
                            } else {
                                if (globals.getRoverCommunicationStatus() == globals.FULL_COMMUNICATION) {
                                    setTitle(getString(R.string.connect) + "  " + globals.getSSID() + " (" + globals.getRssi() + ")");
                                    color = R.color.action_bar_blue;
                                    backgroundDrawable = getApplicationContext().getResources().getDrawable(color);
                                    actionBar.setBackgroundDrawable(backgroundDrawable);
                                } else {
                                    setTitle(getString(R.string.disconnect) + "  " + globals.getSSID());
                                    color = R.color.action_bar_red;
                                    backgroundDrawable = getApplicationContext().getResources().getDrawable(color);
                                    actionBar.setBackgroundDrawable(backgroundDrawable);
                                }
                            }
                            count = 0;
                        }

                        roverLatitudeView.setText(getText(R.string.latitude) + ": " + String.valueOf(globals.getRoverLatitude()));
                        roverLongitudeView.setText(getText(R.string.longitude) + ": " + String.valueOf(globals.getRoverLongitude()));

                        int batteryLevel = globals.getRoverBattery();
                        if(batteryLevel > 80) {
                            batteryArc.setColor(0, 0, 255, 255);
                        } else if(batteryLevel > 60) {
                            batteryArc.setColor(11, 218, 81, 255);
                        } else if(batteryLevel > 20) {
                            batteryArc.setColor(255, 140, 0, 255);
                        } else {
                            batteryArc.setColor(255, 0, 0, 255);
                        }
                        AnimationArc batteryAnimation = new AnimationArc(batteryArc, batteryLevel * 360 / 100);
                        batteryAnimation.setDuration(animationPeriod);
                        batteryArc.startAnimation(batteryAnimation);
                        roverBatteryView.setText(String.valueOf(globals.getRoverBattery()) + "%");

                        if(startFlag) {
                            long time = System.currentTimeMillis() - startTime;
                            time /= 1000;
                            long hh = time / 3600;
                            time %= 3600;
                            long mm = time / 60;
                            time %= 60;
                            long ss = time;
                            String timeText = "";

                            if(hh > 0) {
                                timeText += String.valueOf(hh) + " 時間 ";
                            }
                            timeText += String.valueOf(mm) + " 分 " + String.valueOf(ss) + " 秒";

                            sequenceTimeView.setText(getText(R.string.elapsedTime) + ": " + timeText);
                        }

                        //distanceView.setText(getText(R.string.distance) + ": " + globals.getDistance() + "m");
                        circuitBatteryView.setText(getText(R.string.circuitBattery) + ": " + String.format("%.2f", globals.getCircuitBattery()) + "V");
                        String[] status = {"未実装"};
                        statusView.setText(status[globals.getRoverStatus()]);
                        distanceView.setText(getText(R.string.distance) + ": 未実装");
                    }
                });
            }
        }, 0, 1000);
    }
}
